package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Polynomial;
import viewPolynomials.ViewPol;

public class Controller {

	private Polynomial m_polynomial;
	private ViewPol m_view;

	Controller(Polynomial myModel, ViewPol view){
		this.m_polynomial = myModel;
		this.m_view = view;
		
		//add listeners to the view 
		view.addAdditionListener(new AdditionListener());
		view.addSubstractionListener(new SubstractListener());
		view.addMultiplyListener(new MultiplyListener());
		view.addDivideListener(new DivideListener());
		view.addDerivateListener(new DerivateListener());
		view.addIntegrateListener(new IntegrateListener());

	}
	
	class AdditionListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			String input1 = "";
			String input2 = "";
			try{
				input1 = m_view.getPol1();
			}
			catch(Exception e1) {
				m_view.showError1("Bad input: '" + input1 + "'");
			}
			
			try{
				input2 = m_view.getPol2();
			}
			catch(Exception e2) {
				m_view.showError2("Bad input: '" + input2 + "'");
			}
			//String input1 = m_view.getPol1();
			//String input2 = m_view.getPol2();
			Polynomial p1 = new Polynomial(input1);
			Polynomial p2 = new Polynomial(input2);
			m_view.setAdditionResult(p1.resultToString(p1.addition(p2)));
		}
	}
	
	class SubstractListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			String input1 = m_view.getPol1();
			String input2 = m_view.getPol2();
			Polynomial p1 = new Polynomial(input1);
			Polynomial p2 = new Polynomial(input2);
			m_view.setSubstractionResult(p1.resultToString(p1.substraction(p2)));
		}
	}
	
	class MultiplyListener implements ActionListener {
		public void actionPerformed(ActionEvent e)
		{
			String input1 = m_view.getPol1();
			String input2 = m_view.getPol2();
			Polynomial p1 = new Polynomial(input1);
			Polynomial p2 = new Polynomial(input2);
			m_view.setMultiplicationResult(p1.resultToString(p1.multiply(p2)));
		}
	}
	
	class DivideListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			String input1 = m_view.getPol1();
			String input2 = m_view.getPol2();
			Polynomial p1 = new Polynomial(input1);
			Polynomial p2 = new Polynomial(input2);
			m_view.setDivisionResult(p1.resultToString(p1.divide(p2)));
		}
	}
	
	class DerivateListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			String input1 = m_view.getPol1();
			String input2 = m_view.getPol2();
			Polynomial p1 = new Polynomial(input1);
			Polynomial p2 = new Polynomial(input2);
			m_view.setDerivationResult(p1.resultToString(p1.derivate()) + p2.resultToString(p2.derivate()));
		}
	}
	
	class IntegrateListener implements ActionListener{
		public void actionPerformed(ActionEvent e)
		{
			String input1 = m_view.getPol1();
			String input2 = m_view.getPol2();
			Polynomial p1 = new Polynomial(input1);
			Polynomial p2 = new Polynomial(input2);
			m_view.setIntegrationResult(p1.resultToString(p1.integrate()) + p2.resultToString(  p2.integrate()));
		}
	}


}
