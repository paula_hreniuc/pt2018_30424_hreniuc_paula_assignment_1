package controller;

import model.Polynomial;
import viewPolynomials.ViewPol;

public class PolynomialsOp {

	public static void main(String[] args) {
		Polynomial model = new Polynomial();
		ViewPol view = new ViewPol(model);
		Controller control = new Controller(model, view);
       
        view.setVisible(true);

	}

}
