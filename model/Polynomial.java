package model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Polynomial  {

	private List<Monom> myPolynom = new ArrayList<Monom>();

	public Polynomial(){
		
	}
	
	public Polynomial(String polynom) {

		String[] coefficients = polynom.split(",");

		int pow = -1;
		double coeff = 0;
		for (String result: coefficients) {
			coeff = Double.parseDouble(String.valueOf(result));
			pow++;
			Monom m = new Monom(coeff, pow);
			myPolynom.add(m);
		}
		//printPolynom(myPolynom); // it is just for testing

	}

	public List<Monom> addition(Polynomial otherPolynom){
		//first we check which is the smaller polynomial do the addition and than complete with the rest from the bigger one
		int i, n, max;
		List<Monom> resultAdd = new ArrayList<Monom>();
		Monom res ;
		if (myPolynom.size() <= otherPolynom.getPolynomial().size()) {
			n = myPolynom.size();
			max = otherPolynom.getPolynomial().size();
		}
		else {
			n = otherPolynom.getPolynomial().size();
			max = myPolynom.size();
		}

		for (i = 0; i < n; i++) {
			//we add coefficients 
			res = new Monom(myPolynom.get(i).coeficient + otherPolynom.getPolynomial().get(i).coeficient, myPolynom.get(i).power);
			resultAdd.add(res);
		}

		//now we add the remaining element from the longer polynomial 
		//if they are equal the the for loop won't execute
		for (i = n ; i < max; i++) {
			if (myPolynom.size() < otherPolynom.getPolynomial().size()) {
				res = new Monom (otherPolynom.getPolynomial().get(i).coeficient, otherPolynom.getPolynomial().get(i).power);
				resultAdd.add(res);
			}
			else {
				res = new Monom (myPolynom.get(i).coeficient, myPolynom.get(i).power);
				resultAdd.add(res);
			}
		}
		return resultAdd;
	}

	public List<Monom> substraction(Polynomial otherPolynom){
		int i, n, max;
		List<Monom> resultSub = new ArrayList<Monom>();
		Monom res ;
		if (myPolynom.size() <= otherPolynom.getPolynomial().size()) {
			n = myPolynom.size();
			max = otherPolynom.getPolynomial().size();
		}
		else {
			n = otherPolynom.getPolynomial().size();
			max = myPolynom.size();
		}

		for (i = 0; i < n; i++) {
			//we add coefficients 
			res = new Monom(myPolynom.get(i).coeficient - otherPolynom.getPolynomial().get(i).coeficient, myPolynom.get(i).power);
			resultSub.add(res);
		}

		//now we add the remaining element from the longer polynomial 
		//if they are equal the for loop won't execute
		for (i = n ; i < max; i++) {
			if (myPolynom.size() < otherPolynom.getPolynomial().size()) {
				res = new Monom ((-1)*otherPolynom.getPolynomial().get(i).coeficient, otherPolynom.getPolynomial().get(i).power);
				resultSub.add(res);
			}
			else {
				res = new Monom (myPolynom.get(i).coeficient, myPolynom.get(i).power);
				resultSub.add(res);
			}
		}
		return resultSub;
	}

	public List<Monom> multiply(Polynomial otherPolynom){
		int i , j;
		List<Monom> resultMul = new ArrayList<Monom>();
		for (i = 0; i < otherPolynom.getPolynomial().size() + myPolynom.size() - 1 ; i++) {
			resultMul.add(new Monom(0,0));
		}

		for( i = 0; i <  myPolynom.size(); i++){
			for(j = 0; j < otherPolynom.getPolynomial().size(); j++){
				resultMul.get(i+j).coeficient += myPolynom.get(i).coeficient * otherPolynom.getPolynomial().get(j).coeficient;
				resultMul.get(i+j).power = i + j;
			}
		}
		return resultMul;
	}

	public List<Monom> divide(Polynomial otherPolynom){
		List<Monom> resultDiv = new ArrayList<Monom>();
		return resultDiv;
	}

	public List<Monom> integrate(){
		List<Monom> resultIntegrate = new ArrayList<Monom>();
		int i;
		for (i = 0; i <= myPolynom.size() ; i++) {
			resultIntegrate.add(new Monom(0,0));
		}

		for( i = 0; i < myPolynom.size(); i++){
			resultIntegrate.get(i + 1).coeficient = myPolynom.get(i).coeficient / (i+1);
			resultIntegrate.get(i + 1).power = i + 1;
		}
		return resultIntegrate;
	}


	public List<Monom> derivate(){
		List<Monom> resultDerivate = new ArrayList<Monom>();
		int i;
		for (i = 0; i < myPolynom.size() - 1; i++) {
			resultDerivate.add(new Monom(0,0));
		}
		//the monom with x^0 will disappear that's why we start with i = 1
		for( i = 1; i < myPolynom.size(); i++){
			resultDerivate.get(i - 1).coeficient = myPolynom.get(i).coeficient * i;
			resultDerivate.get(i - 1).power = i -1;
		}
		return resultDerivate;
	}

	public void setPolynomial(List<Monom> myPolynom) {
		this.myPolynom = myPolynom;
	}

	public List<Monom> getPolynomial() {
		return myPolynom;
	}

	//extra method - print coefficient and the power of x for each monom
	public static void printPolynom (List<Monom> myPolynom) {
		for ( int i = 0; i < myPolynom.size(); i++)
		{
			System.out.println(myPolynom.get(i).coeficient + " " + myPolynom.get(i).power);
		}
		System.out.println("\n");

	}
	//extra method - printing for testing in Polynomial class itself
	public static void printPolToString(List<Monom> myPolynom) {

		String listString = "";
		int i = 0;
		for (Monom s : myPolynom)
		{
			if (s.coeficient > 0) {
				listString += s.coeficient + "x^" + s.power;
			}
			else {
				listString += "(" + s.coeficient + ")" + "x^" + s.power ;

			}
			if (i < myPolynom.size()-1 ) {listString += "+";}
			i++;
		}
		listString += "; ";
		System.out.println(listString);
		System.out.println("\n");
	}

	public static String resultToString(List<Monom> myPolynom) {
		String listString = "";
		int i = 0;
		for (Monom s : myPolynom)
		{
			if (s.coeficient >= 0) {
				listString += s.coeficient + "x^" + s.power ;
			}
			else {
				listString += "(" + s.coeficient + ")" + "x^" + s.power ;

			}
			if (i < myPolynom.size()-1) {listString += "+";}
			i++;
		}
		listString += ";";
		return listString;
	}

}
