package model;

public class Monom {
	public double coeficient;
	public int power;
	
	
	public Monom(double coeff, int power) {
		this.coeficient = coeff;
		this.power = power;
	}
	
	public void setMonom(double coeff, int power) {
		this.coeficient = coeff;
		this.power = power;
	}
	
	public double getCoefficient() {
		return coeficient;
	}
	
	public int getPower() {
		return power;
	}
	
}
