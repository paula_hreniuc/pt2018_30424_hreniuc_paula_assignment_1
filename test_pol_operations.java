package PT2018.demo.tet;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Polynomial;

public class test_pol_operations {

	Polynomial p1 = new Polynomial("1,6");
	Polynomial p2 = new Polynomial("4");
	Polynomial p3 = new Polynomial("19,0,0,-6,7");
	Polynomial p4 = new Polynomial("0,45,3");
	//test for the first pair of polynomials
	String resAdd = "5.0x^0+6.0x^1;";
	String resSub = "(-3.0)x^0+6.0x^1;";
	String resMul = "4.0x^0+24.0x^1;";
	String resDiv = ";";
	String resDerivate = "6.0x^0;;";
	String resIntegrate = "0.0x^0+1.0x^1+3.0x^2;0.0x^0+4.0x^1;";
	//test for the second pair of polynomials 
	String resAdd2 = "19.0x^0+45.0x^1+3.0x^2+(-6.0)x^3+7.0x^4;";
	String resSub2 = "19.0x^0+(-45.0)x^1+(-3.0)x^2+(-6.0)x^3+7.0x^4;";
	String resMul2 = "0.0x^0+855.0x^1+57.0x^2+0.0x^3+(-270.0)x^4+297.0x^5+21.0x^6;";
	String resDiv2 = ";";
	String resDerivate2 = "0.0x^0+0.0x^1+(-18.0)x^2+28.0x^3;45.0x^0+6.0x^1;";
	String resIntegrate2 = "0.0x^0+19.0x^1+0.0x^2+0.0x^3+(-1.5)x^4+1.4x^5;0.0x^0+0.0x^1+22.5x^2+1.0x^3;";
	
	@Test
	public void testAdditon() {
		assertEquals(resAdd, p1.resultToString(p1.addition(p2)));
		assertEquals(resAdd2, p3.resultToString(p3.addition(p4)));

	}
	
	@Test
	public void testSubstraction() {
		assertEquals(resSub, p1.resultToString(p1.substraction(p2)));
		assertEquals(resSub2, p3.resultToString(p3.substraction(p4)));

	}
	
	@Test
	public void testMultiplication() {
		assertEquals(resMul, p1.resultToString(p1.multiply(p2)));
		assertEquals(resMul2, p3.resultToString(p3.multiply(p4)));
	}
	
	@Test
	public void testDivide() {
		assertEquals(resDiv, p1.resultToString(p1.divide(p2)));
		assertEquals(resDiv2, p3.resultToString(p3.divide(p4)));

	}
	
	@Test
	public void testDerivate() {
		assertEquals(resDerivate, p1.resultToString(p1.derivate()) + p2.resultToString(p2.derivate()));
		assertEquals(resDerivate2, p3.resultToString(p3.derivate()) + p4.resultToString(p4.derivate()));

	}
	
	@Test
	public void testIntegrate() {
		assertEquals(resIntegrate, p1.resultToString(p1.integrate()) + p2.resultToString(p2.integrate()));
		assertEquals(resIntegrate2, p3.resultToString(p3.integrate()) + p4.resultToString(p4.integrate()));

	}
}
