package viewPolynomials;

import java.awt.ComponentOrientation;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Polynomial;

public class ViewPol extends JFrame {
	private static final long serialVersionUID = 1L;

	JFrame frame  = new JFrame ("Operations on Polynomials");
	JPanel panel = new JPanel();
	JLabel pol1 = new JLabel("Polynom 1: ", JLabel.RIGHT);
	JLabel pol2 = new JLabel("Polynom 2: ", JLabel.RIGHT);
	public  JButton button1 = new JButton("Add");
	public  JButton button2 = new JButton("Substract");
	public  JButton button3 = new JButton("Multiply");
	public  JButton button4 = new JButton("Divide");
	public  JButton button5 = new JButton("Derivate");
	public  JButton button6 = new JButton("Integrate");	
	public  JTextField tf1 = new JTextField(" ");
	public  JTextField tf2 = new JTextField(" ");
	public  JTextField tf3 = new JTextField(" ");
	public  JTextField tf4 = new JTextField(" ");
	public  JTextField tf5 = new JTextField(" ");
	public  JTextField tf6 = new JTextField(" ");
	public  JTextField tf7 = new JTextField(" ");
	public  JTextField tf8 = new JTextField(" ");
	JLabel tf9 = new JLabel("*Introduce the coefficients of the polynomials", JLabel.RIGHT);
	JLabel tf10 = new JLabel(" with character ',' between them.");
	JLabel tf11 = new JLabel("*****First coeff. introduced is the coeff. of x^0", JLabel.RIGHT);
	JLabel tf12 = new JLabel(" ex: 1, 0, 2 => 1*x^0 + 0*x^1 + 2*x^2");


	public ViewPol(Polynomial model) {
		frame.add(panel);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 300);
		frame.setVisible(true);

		initialize();
	}


	private void initialize() {
		panel.setLayout(new GridLayout(10, 2, 5, 10));
		panel.add(pol1);
		panel.add(tf1);
		panel.add(pol2);
		panel.add(tf2);
		panel.add(button1);
		panel.add(tf3);
		panel.add(button2);
		panel.add(tf4);
		panel.add(button3);
		panel.add(tf5);
		panel.add(button4);
		panel.add(tf6);
		panel.add(button5);
		panel.add(tf7);
		panel.add(button6);
		panel.add(tf8);
		panel.add(tf9);
		panel.add(tf10);
		panel.add(tf11);
		panel.add(tf12);
		panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
	}

	public  String getPol1() {
		return tf1.getText(); //get polynom 1
	}

	public  String getPol2() {
		return tf2.getText(); //get polynom 1
	}

	public  void setAdditionResult(String addResult) {
		tf3.setText(addResult);
	}

	public void setSubstractionResult(String subResult) {
		tf4.setText(subResult);
	}

	public void setMultiplicationResult(String mulResult) {
		tf5.setText(mulResult);
	}

	public void setDivisionResult(String divResult) {
		tf6.setText(divResult);
	}

	public void setDerivationResult(String derivResult) {
		tf7.setText(derivResult);
	}

	public void setIntegrationResult(String integrationRes) {
		tf8.setText(integrationRes);
	}

	public void showError1(String error) {
		tf1.setText(error);
	}
	
	public void showError2(String error) {
		tf2.setText(error);
	}
	
	//////////for controller
	public void addAdditionListener(ActionListener e) {
		button1.addActionListener(e);
	}
	public void addSubstractionListener(ActionListener e) {
		button2.addActionListener(e);
	}
	public void addMultiplyListener(ActionListener e) {
		button3.addActionListener(e);
    }
	public void addDivideListener(ActionListener e) {
		button4.addActionListener(e);
    }
	public void addDerivateListener(ActionListener e) {
		button5.addActionListener(e);
    }
	public void addIntegrateListener(ActionListener e) {
		button6.addActionListener(e);
    }
	
/*	public static void main (String args[]) {
		new ViewPol();
	}*/
}
